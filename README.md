# smartScanner 1.1.02 vs 1.0.6

1.1.02 时间范围 2023-08-08-----2023-08-16 


#### 1.1.02 错误统计
|  错误类型   | ANR  | Null  | 
|  ----  | ----  |  ----  |  
| 次数  | 4次 | 2次 | 



业务类ANR 
<br/>
FeedbackImgAdapter.onCreateViewHolder，未发现特殊写法

业务类Null
<br/>
GoogleBookResponse.getTotalItems()' on a null object reference


1.0.06 时间范围 2023-08-08-----2023-08-16 
